//
//  HeroeCollectionViewCell.swift
//  HeroBook
//
//  Created by CarlosEMG on 9/5/19.
//  Copyright © 2019 CarlosEMG. All rights reserved.
//

import UIKit

class HeroeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgHeroe: UIImageView!
    
    override func draw(_ rect: CGRect) {
        self.imgHeroe.layer.cornerRadius = 5.0
        self.imgHeroe.clipsToBounds = true
        super.draw(rect)
    }
}
