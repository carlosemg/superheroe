//
//  ConexionService.swift
//  HeroBook
//
//  Created by CarlosEMG on 9/5/19.
//  Copyright © 2019 CarlosEMG. All rights reserved.
//

import UIKit

protocol RequestDelegate: class {
    
    func didFinishRequest( withResponse response:Codable)
    
    func didFailRequest()
    
    func didFinishDownloadImage( image:UIImage, forUrl name:String )
    
}

class ConexionService<T:Codable>: NSObject {

    fileprivate var responseObject:T?
    var requestDelegate:RequestDelegate?
    fileprivate var imgResource:[String:UIImage] = [:]
    
    func requestInfo(forUrl urlString:String) {
        let requestHero = RequestInfo(urlBase: urlString, requestType: REQUEST_TYPE.GET, needsResponse: true)
        ConexionCore.sharedInstance.sendSimpleRequest(info: requestHero) { [weak self]
            (success, info) in
            if success {
                self?.parseInfoResponse(info: info!)
            } else {
                self?.requestDelegate?.didFailRequest()
            }
        }
    }
    
    fileprivate func parseInfoResponse( info: String) {
        if let jsonData = info.data(using: .utf8)
        {
            if let decode = try? JSONDecoder().decode(T.self, from: jsonData) {
                responseObject = decode
                self.requestDelegate?.didFinishRequest(withResponse: responseObject)
            }
        }
    }
    
    func requestImage(forUrl urlString:String) {
        if let image = imgResource[urlString] {
            self.requestDelegate?.didFinishDownloadImage(image: image, forUrl: urlString)
            
        } else {
            ConexionCore.sharedInstance.getImage(atUrl: urlString) { [weak self]
                (success, image) in
                if success && image != nil {
                    self?.imgResource[urlString] = image!
                    self?.requestDelegate?.didFinishDownloadImage(image: image!, forUrl: urlString)
                } else {
                    self?.requestDelegate?.didFailRequest()
                }
            }
        }
    }
    
    func getSavedImage(forUrl urlString:String) -> UIImage?{
        return imgResource[urlString]
    }
    
}
