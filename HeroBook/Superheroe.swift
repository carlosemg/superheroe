//
//  Superheroe.swift
//  HeroBook
//
//  Created by CarlosEMG on 9/5/19.
//  Copyright © 2019 CarlosEMG. All rights reserved.
//

import UIKit

class Superheroe: Codable {

    var height:String = ""
    var groups:String = ""
    var realName:String = ""
    var power:String = ""
    var photo:String = ""
    var name:String = ""
    var abilities:String = ""
    
    enum CodingKeys:String, CodingKey {
        case height = "height"
        case groups = "groups"
        case realName = "realName"
        case power = "power"
        case photo = "photo"
        case name = "name"
        case abilities = "abilities"
    }
}

class Superheroes: Codable {
    
    var superheroes:[Superheroe]
    
    init() {
        self.superheroes = []
    }
    
    enum CodingKeys:String, CodingKey {
        case superheroes = "superheroes"
    }
    
}
