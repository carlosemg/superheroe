//
//  HeroViewController.swift
//  HeroBook
//
//  Created by CarlosEMG on 9/5/19.
//  Copyright © 2019 CarlosEMG. All rights reserved.
//

import UIKit

let CELL_IDENTIFIER = "HeroeCollectionViewCell"

class HeroViewController: UIViewController {
    
    @IBOutlet weak var loadingView: UIVisualEffectView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imgHeroe: UIImageView!
    @IBOutlet weak var lblNameHeroe: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblGroups: UILabel!
    @IBOutlet weak var lblPowers: UILabel!
    @IBOutlet weak var lblHabilities: UILabel!
    @IBOutlet weak var cnstImgHeroe: NSLayoutConstraint!
    
    private let heroPresenter = HeroPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        heroPresenter.loadHeroes()
        heroPresenter.heroeDelegate = self
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.layer.borderColor = UIColor.lightGray.cgColor
        collectionView.layer.borderWidth = 1.0
        collectionView.layer.cornerRadius = 5.0
    }

    func updateCurrentHero( atPosition position:Int ) {
        if let heroe = heroPresenter.getCurrentHeroe(forPosition: position) {
            imgHeroe.image = heroPresenter.getImage(atPosition: position)
            lblNameHeroe.text = heroe.name + "\n( " + heroe.realName + " )"
            lblHeight.text = heroe.height
            lblGroups.text = heroe.groups
            lblPowers.text = heroe.power
            lblHabilities.text = heroe.abilities
        }
    }
}

extension HeroViewController: HeroPresenterDelegate {
    
    func finishLoadHeroes() {
        self.loadingView.isHidden = true
        self.collectionView.reloadData()
        updateCurrentHero(atPosition: 0)
    }
    
    func setImage(image:UIImage, forCellAtPosition position:Int) {
        self.collectionView.reloadData()
        if imgHeroe.image == nil {
            updateCurrentHero(atPosition: 0)
        }
    }
}

extension HeroViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        updateCurrentHero(atPosition: indexPath.row)
    }
}

extension HeroViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return heroPresenter.getNumberHeroes()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CELL_IDENTIFIER, for: indexPath) as! HeroeCollectionViewCell
        if let image = heroPresenter.getImage(atPosition: indexPath.row) {
            cell.imgHeroe.image = image
        } else {
            heroPresenter.requestForImage(atPosition: indexPath.row)
        }
        return cell
    }
}
