//
//  HeroPresenter.swift
//  HeroBook
//
//  Created by CarlosEMG on 9/5/19.
//  Copyright © 2019 CarlosEMG. All rights reserved.
//

import UIKit

fileprivate let BASE_SUPERHERO_URL = "https://api.myjson.com/bins/bvyob"

protocol HeroPresenterDatasource:class {
    
    func getNumberHeroes() -> Int
    func getCurrentHeroe( forPosition position:Int) -> Superheroe?
    func getImage( atPosition position:Int) -> UIImage?
    func requestForImage( atPosition position:Int)
}

protocol HeroPresenterDelegate:class {
    
    func finishLoadHeroes()
    func setImage(image:UIImage, forCellAtPosition position:Int)
}

class HeroPresenter: NSObject {
    
    fileprivate var serviceRequest:ConexionService<Superheroes>
    
    fileprivate var heroes: Superheroes = Superheroes()
    fileprivate var listImg:[String:Int] = [:]
    weak var heroeDelegate: HeroPresenterDelegate?
    
    override init() {
        serviceRequest = ConexionService<Superheroes>()
        super.init()
        
        serviceRequest.requestDelegate = self
    }

    func loadHeroes() {
        serviceRequest.requestInfo(forUrl: BASE_SUPERHERO_URL)
    }
    
    func getNumberHeroes() -> Int {
        return heroes.superheroes.count
    }
    
    func getCurrentHeroe(forPosition position: Int) -> Superheroe? {
        return heroes.superheroes.count >= position ? heroes.superheroes[position] : nil
    }
    
    func requestForImage( atPosition position:Int)  {
        guard heroes.superheroes.count >= position else {
            return
        }
        let url = heroes.superheroes[position].photo
        listImg[url] = position
        serviceRequest.requestImage(forUrl: url)
    }
    
    func getImage( atPosition position:Int) -> UIImage? {
        let url = heroes.superheroes[position].photo
        return serviceRequest.getSavedImage(forUrl: url)
    }
    
}

extension HeroPresenter: RequestDelegate {
    
    func didFinishDownloadImage(image: UIImage, forUrl name: String) {
        DispatchQueue.main.async {
            guard let position = self.listImg[name] else {
                return
            }
            self.heroeDelegate?.setImage(image: image, forCellAtPosition: position)
        }
    }
    
    func didFinishRequest(withResponse response: Codable) {
        heroes = response as! Superheroes
        DispatchQueue.main.async {
            self.heroeDelegate?.finishLoadHeroes()
        }
    }
    
    func didFailRequest() {
        DispatchQueue.main.async {
            self.heroeDelegate?.finishLoadHeroes()
        }
    }
}

